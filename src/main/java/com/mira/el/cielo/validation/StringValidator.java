package com.mira.el.cielo.validation;

/**
 * Created by pablo on 16/03/17.
 */
public class StringValidator
{
    public static boolean validate(String value)
    {
        return !(value == null || value.isEmpty());
    }

    public static boolean isValid(String value)
    {
        return StringValidator.validate(value);
    }

    public static boolean fails(String value)
    {
        return !StringValidator.validate(value);
    }
}
