package com.mira.el.cielo;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

/**
 * Created by pablo on 14/03/17.
 */
public class CakesHTMLRenderer
{
    private final Writer target;
    private final CakeEntityHTMLRenderer cakeRenderer;

    public CakesHTMLRenderer(Writer target)
    {
        this.target = target;
        cakeRenderer = new CakeEntityHTMLRenderer(this.target);
    }

    public void renderAsUnorderedList(Collection<CakeEntity> cakes) throws IOException
    {
        target.write("<ul>");

        for (CakeEntity cake: cakes)
        {
            target.write("<li>");
            cakeRenderer.render(cake);
            target.write("</li>");
        }
        target.write("</ul>");
    }
}


