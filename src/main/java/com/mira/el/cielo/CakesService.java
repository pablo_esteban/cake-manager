package com.mira.el.cielo;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

/**
 * Created by pablo on 20/03/17.
 */
public class CakesService
{
    public static void loadJsonFrom(String url)
    {
        Collection<CakeEntity> cakes = JsonUtils.parseCakesFromJson(url);
        save(cakes);
    }

    public static Collection<CakeEntity> list()
    {
        return StorageUtils.fetchAll();
    }

    public static void save(Collection<CakeEntity> cakes)
    {
        StorageUtils.save(cakes);
    }

    public static void save(CakeEntity cake)
    {
        StorageUtils.save(cake);
    }

    public static void writeAllAsJson(Writer target)
    {
        Collection<CakeEntity> cakes = list();
        JsonUtils.serialiseCakesAsJson(target, cakes);
    }

    public static void writeAllAsHtmlList(Writer target)
    {
        try
        {
            Collection<CakeEntity> cakes = list();
            CakesHTMLRenderer renderer = new CakesHTMLRenderer(target);
            renderer.renderAsUnorderedList(cakes);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e.toString(), e);
        }
    }
}
