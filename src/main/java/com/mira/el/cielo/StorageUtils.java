package com.mira.el.cielo;

import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;

import java.util.Collection;
import java.util.List;

/**
 * Created by pablo on 21/03/17.
 */
public class StorageUtils
{
    public static void save(Collection<CakeEntity> cakes)
    {
        Session session = HibernateUtil.openSession();
        try
        {
            session.beginTransaction();
            for (CakeEntity cake: cakes)
            {
                session.persist(cake);
            }
            session.getTransaction().commit();
            session.close();
        }
        catch (ConstraintViolationException e)
        {
            throw new RuntimeException(e.toString(), e);
        }
    }

    public static void save(CakeEntity cake)
    {
        Session session = HibernateUtil.openSession();
        try
        {
            session.beginTransaction();
            session.persist(cake);
            session.getTransaction().commit();
            session.close();
        }
        catch (ConstraintViolationException e)
        {
            throw new RuntimeException(e.toString(), e);
        }
    }

    public static Collection<CakeEntity> fetchAll()
    {
        try
        {
            Session session = HibernateUtil.openSession();
            Collection<CakeEntity> cakes = session.createCriteria(CakeEntity.class).list();
            session.close();
            return cakes;
        } catch (ConstraintViolationException ex) {
            throw new RuntimeException(ex);
        }
    }
}
