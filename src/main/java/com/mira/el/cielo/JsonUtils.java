package com.mira.el.cielo;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.List;

/**
 * Created by pablo on 21/03/17.
 */
public class JsonUtils
{
    public static Collection<CakeEntity> parseCakesFromJson(String url)
    {
        try (InputStream inputStream = new URL(url).openStream())
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String loadedJsonString = reader.readLine();
            ObjectMapper mapper = new ObjectMapper();
            Collection<CakeEntity> cakes = mapper.readValue(loadedJsonString, new TypeReference<List<CakeEntity>>() { });
            return cakes;
        }
        catch (MalformedURLException e)
        {
            throw new RuntimeException(e.toString(), e);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e.toString(), e);
        }
    }

    public static void serialiseCakesAsJson(Writer target, Collection<CakeEntity> cakes)
    {
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(target, cakes);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e.toString(), e);
        }
    }
}
