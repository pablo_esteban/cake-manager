package com.mira.el.cielo;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by pablo on 14/03/17.
 */
@WebServlet("/cakes")
public class CakesFunction extends HttpServlet
{
    private static final long serialVersionUID = 4L;

    protected void showCakes(HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.print("<html><body>");
        CakesService.writeAllAsHtmlList(out);
        out.print("</body></html>");
    }

    protected void deliverJsonCakes(HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        CakesService.writeAllAsJson(out);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String contentType = request.getContentType();
        if ("application/json".equals(contentType))
        {
            deliverJsonCakes(response);
        }
        else
        {
            showCakes(response);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String contentType = request.getContentType();
        System.out.println(contentType);
        if ("application/json".equals(contentType))
        {
            addJsonCake(request, response);
        }
        else
        {
            addFormCake(request, response);
        }
    }

    protected void addJsonCake(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ObjectMapper mapper = new ObjectMapper();
        CakeEntity cake = mapper.readValue(request.getInputStream(),CakeEntity.class);
        CakesService.save(cake);
        System.out.println("Just added a json Cake!");
        response.setStatus(200);
    }

    protected void addFormCake(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String title = request.getParameter("title");
        String desc = request.getParameter("desc");
        String image = request.getParameter("image");
        System.out.println(request.getContentType());

        CakeEntity cake = new CakeEntity();
        cake.title = title;
        cake.desc = desc;
        cake.image = image;
        List<String> violations = cake.validate();
        if (!violations.isEmpty()) {
            request.setAttribute("violations", violations);
            request.setAttribute("title", title);
            request.setAttribute("desc", desc);
            request.setAttribute("image", image);
            request.getRequestDispatcher("/add.jsp").forward(request, response);
            return;
        }

        CakesService.save(cake);
        System.out.println("Just added a form Cake!");
        showCakes(response);
    }
}
