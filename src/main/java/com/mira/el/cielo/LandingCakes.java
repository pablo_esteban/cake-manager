package com.mira.el.cielo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Collection;

@WebServlet("/")
public class LandingCakes extends HttpServlet
{
    private static final long serialVersionUID = 3L;
    private static final String dataSource = "https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json";
    private Collection<CakeEntity> cakes;

    public void init()
    {
        System.out.println("init started");
        CakesService.loadJsonFrom(dataSource);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.print("<html><body>");
        out.print("<a href=\"add.jsp\">Add a Cake</a>");
        CakesService.writeAllAsHtmlList(out);
        out.print("<a href=\"add.jsp\">Add a Cake</a>");
        out.print("</body></html>");
    }
}

