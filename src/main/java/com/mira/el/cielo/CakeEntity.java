package com.mira.el.cielo;

import com.mira.el.cielo.validation.StringValidator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by pablo on 13/03/17.
 */
@Entity
@Table(name = "Cakes", uniqueConstraints = {@UniqueConstraint(columnNames = "ID")})
public class CakeEntity implements Serializable
{
    private static final long serialVersionUID = -1798070234593154676L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer cakeId;

    public String title;
    public String desc;
    public String image;

    public List<String> validate() {
        List<String> violations = new LinkedList<>();
        if (StringValidator.fails(title)) {
            violations.add("Title cannot be empty");
        }
        if (StringValidator.fails(desc)) {
            violations.add("Description cannot be empty");
        }
        if (StringValidator.fails(image)) {
            violations.add("Image URL cannot be empty");
        }
        return violations;
    }


}
