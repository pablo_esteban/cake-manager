package com.mira.el.cielo;

import java.io.IOException;
import java.io.Writer;

/**
 * Created by pablo on 14/03/17.
 */
public class CakeEntityHTMLRenderer
{
    private final Writer target;

    public CakeEntityHTMLRenderer(Writer target)
    {
        this.target = target;
    }

    public void render(CakeEntity cake) throws IOException
    {
        target.write("<p>" + cake.title);
        target.write("<img src='" + cake.image + "' />");
        target.write("</p>");
    }
}