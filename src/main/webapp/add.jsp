<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add a Cake</title>
</head>
<body>
    <c:if test="${violations != null}">
        <c:forEach items="${violations}" var="violation">
            <p>${violation}.</p>
        </c:forEach>
    </c:if>
    <form action="${pageContext.request.contextPath}/cakes" method="post">
        <label for="title">Title:</label><input type="text" name="title" id="title" value="${title}"></br>
        <label for="desc">Description:</label><input type="text" name="desc" id="desc" value="${desc}"></br>
        <label for="image">Image:</label><input type="text" name="image" id="image" value="${image}"></br>
        <input type="submit" value="Add">
    </form>
</body>
</html>